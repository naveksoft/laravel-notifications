The package allows you to broadcast notifications to everyone except the current user. (Only To Others laravel events feature)
##### Installation
Add `repositories` in `composer.json`:
```
    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/naveksoft/laravel-notifications.git"
        }
    ]
```
And execute in console to install the package:
`composer require naveksoft/laravel-notifications`
##### Get started
You need to change the laravel class `Illuminate\Notifications\NotificationServiceProvider::class` to ` NavekSoft\Notifications\NotificationServiceProvider::class` in `app.php`
```php
    'providers' => [
        /*
         * Laravel Framework Service Providers...
         */
         //Illuminate\Notifications\NotificationServiceProvider::class - remove this
        NavekSoft\Notifications\NotificationServiceProvider::class,
        
    ],
```
If you want to use custom event class for broadcast event, you will need to define the static method `useBroadcastClass`
in `AppServiceProvider.php`. By default, it will be use `Illuminate\Notifications\Events\BroadcastNotificationCreated`:
```php
    public function boot()
    {
        NavekSoft\Notifications\BroadcastChannel::useBroadcastClass(OtherEvent::class);
    }
```
New class `OtherEvent`  must override `NavekSoft\BroadcastingBroadcastNotificationCreated`
##### Notifications
If you don't want to broadcast message for the current socket connection you need to add  the `InteractsWithSockets` trait, method `dontBroadcastToCurrentUser()` to the constructor  (It will return value from header `X-Socket-ID` in order
to broadcast to all connections except current) and define channel in `via` method.
```php
    
    use Illuminate\Broadcasting\InteractsWithSockets;
    use NavekSoft\Notifications\BroadcastChannel;
    
    class TestNotification extends Notification
    {
        use InteractsWithSockets;
        
        public function __construct()
        {
            $this->dontBroadcastToCurrentUser();
        }
    
        public function via($notifiable)
        {
            return [BroadcastChannel::class];
        }
        
```
If notification creating in job you need to get `X-Socket-ID` value from request in job constructor
```php
namespace App\Jobs;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Broadcast;

class TestJob implements ShouldQueue

    public $socket;

    public function __construct()
    {
        $this->socket = Broadcast::socket();
    }
    
    public function handle()
    {
        $notification = new TestNotification();
        $notification->socket = $this->socket;
    }
```
