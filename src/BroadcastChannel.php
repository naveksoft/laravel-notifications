<?php

namespace NavekSoft\Notifications;

use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Channels\BroadcastChannel as BaseBroadcastChannel;
use Illuminate\Broadcasting\InteractsWithSockets;

class BroadcastChannel extends BaseBroadcastChannel
{
    public static $event = BroadcastNotificationCreated::class;

    public static function useBroadcastClass($event){
        static::$event = $event;
    }

    public function send($notifiable, Notification $notification){
        $message = $this->getData($notifiable, $notification);

        $event = new static::$event(
            $notifiable, $notification, is_array($message) ? $message : $message->data
        );

        if (property_exists($notification, 'socket')) {
            $event->socket = $notification->socket;
        }

        if($message instanceof BroadcastMessage){
            $event->onConnection($message->connection)
                ->onQueue($message->queue);
        }

        return $this->events->dispatch($event);
    }
}