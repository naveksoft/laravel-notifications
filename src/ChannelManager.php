<?php

namespace NavekSoft\Notifications;

use Illuminate\Notifications\ChannelManager as BaseChannelManager;

class ChannelManager extends BaseChannelManager
{
    protected function createBroadcastDriver(){
        return $this->container->make(BroadcastChannel::class);
    }
}