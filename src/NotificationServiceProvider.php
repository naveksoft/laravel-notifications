<?php

namespace NavekSoft\Notifications;

use Illuminate\Contracts\Notifications\Dispatcher as DispatcherContract;
use Illuminate\Contracts\Notifications\Factory as FactoryContract;
use Illuminate\Notifications\ChannelManager as BaseChannelManager;
use Illuminate\Notifications\NotificationServiceProvider as BaseNotificationServiceProvider;

class NotificationServiceProvider extends BaseNotificationServiceProvider
{
    public function register()
    {
        $this->app->singleton(BaseChannelManager::class, function ($app) {
            return new ChannelManager($app);
        });

        $this->app->alias(
            BaseChannelManager::class, DispatcherContract::class
        );

        $this->app->alias(
            BaseChannelManager::class, FactoryContract::class
        );
    }
}