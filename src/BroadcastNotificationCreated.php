<?php

namespace NavekSoft\Notifications;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Notifications\Events\BroadcastNotificationCreated as BaseBroadcastNotificationCreated;

class BroadcastNotificationCreated extends BaseBroadcastNotificationCreated
{
    use InteractsWithSockets;

    public function broadcastAs(){
        return BaseBroadcastNotificationCreated::class;
    }
}